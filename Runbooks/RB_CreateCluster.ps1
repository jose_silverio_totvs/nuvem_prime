workflow RB_CreateCluster
{
    Param(
        [Parameter(Mandatory=$true)]
        [String] $NomeCluster,
        [Parameter(Mandatory=$true)]
        [String] $ServidorPrimario,
        [Parameter(Mandatory=$true)]
        [String] $ServidorSecundario,
        [Parameter(Mandatory=$true)]
        [String] $IP_VIP
        
    )
    
    try {$credencial = Get-AutomationPSCredential -Name "CRED_DBCLOUD_ADMIN" } catch {}
        
    InlineScript{
        
            $cred = $Using:credencial
            $ipvip = $Using:IP_VIP
            $servidor = $Using:ServidorPrimario
            $servidorSecundario = $Using:ServidorSecundario
            $NomeCluster = $Using:NomeCluster
            
            $saida = hostname
            Write-Output "Executando em $saida"
            Write-Output "========================================"
            Write-Output "Criando Cluster"
            Write-Output "========================================"
            Write-Output "Servidor Primário: $servidor"
            Write-Output "Servidor Secundário: $servidorSecundario"
            Write-Output "Nome do Cluster: $NomeCluster"
            Write-Output "IP VIP: $ipvip"
            Write-Output "========================================"
            
            #instalando features de clustering nos nós involvidos
            $cmd={Install-WindowsFeature -Name Failover-Clustering –IncludeManagementTools}
            Invoke-Command -ComputerName $servidor,$servidorSecundario -Credential $cred -ScriptBlock{$cmd}
            
            $cmd={Install-windowsfeature RSAT-Clustering -IncludeAllSubFeature}
            Invoke-Command -ComputerName $servidor,$servidorSecundario -Credential $cred -ScriptBlock{$cmd}
            
            #Crianco um PS Session para executar o NewCluster 
            $k = New-PSSession -Credential $cred
            
            New-Cluster $nomecluster -Node $servidor, $servidorSecundario -NoStorage -StaticAddress $ipvip

    }
    
}