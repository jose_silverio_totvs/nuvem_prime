﻿workflow RB_Install_SQL_Server
{
    
     Param
     (
         [Parameter (Mandatory= $false)]
         [String] $servidor,

         [Parameter (Mandatory= $false)]
         [String] $caminho,
         
         [Parameter (Mandatory= $false)]
         [String] $caminhoBinario,

         [Parameter (Mandatory= $false)]
         [String] $produto,

         [Parameter (Mandatory= $false)]
         [String] $collation,

		 [Parameter (Mandatory= $false)]
         [String] $senhaSA,

		 [Parameter (Mandatory= $false)]
         [String] $contaSQL,

		 [Parameter (Mandatory= $false)]
         [String] $contaSQLPWD,

		 [Parameter (Mandatory= $false)]
         [String] $contaAgente,

		 [Parameter (Mandatory= $false)]
         [String] $contaAgentePWD,

		 [Parameter (Mandatory= $false)]
         [Int] $PortaConexao,

		 [Parameter (Mandatory= $false)]
         [Int] $qtdProcessadorPorInstancia,

		 [Parameter (Mandatory= $false)]
         [String] $admins,

		 [Parameter (Mandatory= $false)]
         [String] $tamanhoTempDB,

		 [Parameter (Mandatory= $false)]
         [String] $growTempDB,

		 [Parameter (Mandatory= $false)]
         [String] $tamanhoTempLog,

		 [Parameter (Mandatory= $false)]
         [String] $growTempLog,

		 [Parameter (Mandatory= $false)]
         [String] $MemoriaAlocadag,

		 [Parameter (Mandatory= $false)]
         [int] $fillFactor,

		 [Parameter (Mandatory= $false)]
         [int] $usaContainedDB,

		 [Parameter (Mandatory= $false)]
         [Int] $MaxDop

     )


	     # credencial para metodo conexao 2, só funciona fora do escopo InlineScript  
	try {$credencial = Get-AutomationPSCredential -Name "CRED_DBCLOUD_ADMIN" } catch {}

	InlineScript {
         
        Import-Module SMA_ExecutaQuery 

		# variaveis fora do bloco InlineScript devem ser referenciadas com $Using        
		$servidor = $Using:servidor
		$chaveProduto = $Using:chaveProduto 
        $caminho = $Using:caminho
        $caminhoBinario = $Using:caminhoBinario
        $produto = $Using:produto
        $collation = $Using:collation
        $senhaSA = $Using:senhaSA
        $contaSQL = $Using:contaSQL
        $contaSQLPWD = $Using:contaSQLPWD
        $contaAgente = $Using:contaAgente
        $contaAgentePWD = $Using:contaAgentePWD
        $PortaConexao = $Using:PortaConexao
        $qtdProcessadorPorInstancia = $Using:qtdProcessadorPorInstancia
        $admins = $Using:admins
        $tamanhoTempDB = $Using:tamanhoTempDB
        $growTempDB = $Using:growTempDB
        $tamanhoTempLog = $Using:tamanhoTempLog
        $growTempLog = $Using:growTempLog
        $MemoriaAlocadag = $Using:MemoriaAlocadag
        $fillFactor = $Using:fillFactor
        $usaContainedDB = $Using:usaContainedDB
        $MaxDop = $Using:MaxDop

        if (!$servidor){$servidor = 'teste_sql_A'}
        if (!$produto){$produto = 'RM'}
        
		if (!$chaveProduto){$chaveProduto = 'TBR8B-BXC4Y-298NV-PYTBY-G3BCP'}
		if (!$caminho){$caminho = 'D:\MSSQL' }
		if (!$caminhoBinario){$caminhoBinario = 'D:\MSSQL_BINN' }

		if($produto.ToUpper() -ccontains 'RM' ){
			$collation = 'SQL_Latin1_General_CP1_CI_AI'
		}ElseIf( $produto.ToUpper() -ccontains 'PR' ){
		    $collation = 'Latin1_General_BIN'}

        if (!$senhaSA){$senhaSA = '8Wrzfs6C0ZQjQEDrMRpI!!!'}
		if (!$contaSQL){$contaSQL = 'dbcloud\vmm_sqlserver'}
		if (!$contaSQLPWD){$contaSQLPWD = '0719ad23f9c02f1a042b9f9314fbe62d!!!'}
		if (!$contaAgente){$contaAgente = 'dbcloud\vmm_sqlserver'}
		if (!$contaAgentePWD){$contaAgentePWD = '0719ad23f9c02f1a042b9f9314fbe62d!!!'}

		if (!$PortaConexao){$PortaConexao = 38000}
		if (!$qtdProcessadorPorInstancia){$qtdProcessadorPorInstancia = 2}

		if (!$admins){$admins = "`"dbcloud\adriano.leite`""}

		if (!$tamanhoTempDB){$tamanhoTempDB = '10MB'}
		if (!$growTempDB){$growTempDB = '10MB'}
		if (!$tamanhoTempLog){$tamanhoTempLog = '10MB'}
		if (!$growTempLog){$growTempLog = '20MB'}
		if (!$MemoriaAlocada){$MemoriaAlocada = '2048'}
		if (!$fillFactor){$fillFactor = 80}
		if (!$usaContainedDB){$usaContainedDB = 1}
		if (!$MaxDop){$MaxDop = 1}
        
        #Constantes
        $instancia = "MSSQLSERVER"
        
        $pastaInstancia = "$caminhoBinario\$instancia" # "$caminho\$instancia"
    
        $pastaArquivos = $pastaInstancia

        $pastaBKP = "$caminho\BACKUP\$instancia" #"$pastaArquivos\BKP"

        $pastaTEMPDB= "$caminho\TEMP01\$instancia" #"$pastaArquivos\TempDB\DATA"
        $pastaTEMPDBLOG= "$caminho\TEMP01\$instancia" #"$pastaArquivos\TempDB\LOG"
        $pastaDB = "$caminho\DATA01\$instancia" #"$pastaArquivos\DATA"
        $pastaLOG = "$caminho\LOG01\$instancia" #"$pastaArquivos\LOG"

		# credencial para metodo conexao 2, só funciona fora do escopo InlineScript
		$credencialinterna = $Using:credencial
        
		#Step 1 - Get ISO path
		try{

			$iso_unit = Invoke-Command -ComputerName $servidor -Credential $credencialinterna -ScriptBlock {Get-WmiObject Win32_LogicalDisk |where {$_.VolumeName -like “SQL201*”}  | Select-Object DeviceID  | ft -hidetableheaders | out-string}
            $iso_unit = $iso_unit.ToString().Trim()        
			write-output "Mount point da midia sql: $iso_unit"
		}
		catch{
			throw "Erro no Step 1 - Get ISO path $_.Exception"
		}
        
        #Step 2 - Check powershell modules
        
        try {
            $command = "Get-Module SqlServer -ListAvailable |select Name | ft -hidetableheaders"
            $command = $ExecutionContext.InvokeCommand.NewScriptBlock($command)
            #$resultado = invoke-Command -ComputerName $servidor -Credential $credencialinterna -ScriptBlock {
            #    $r = Get-Module SqlServer -ListAvailable |select Name | ft -hidetableheaders
            #    Return $r
            #}
            $resultado = invoke-Command -ComputerName $servidor -Credential $credencialinterna -ScriptBlock $command
            $resultado =  $resultado.ToString()
            if ($resultado){
                write-output "Modulo powershell instalado"
            }else{
                throw "Step 2 - Modulo powershell não encontrado"
            } 
            
        }
        catch{
            $resultado = $False
            throw "Step 2 - Check powershell modules $_.Exception"
        }
        
        #Step 3 - Instalacao  SQL
        try {
           # $command = "Start-Process $iso_unit\Setup.exe 
           # -ArgumentList '/ACTION=INSTALL /IACCEPTSQLSERVERLICENSETERMS /Q /ENU /FEATURES=SQLENGINE,FULLTEXT,TOOLS /INSTANCEDIR=`"$caminhoBinario`" $exibeVerbose /INSTANCENAME=`"$instancia`" /PID=`"$chaveProduto`" /INSTALLSQLDATADIR=`"$pastaArquivos`"     /SQLSVCACCOUNT=`"$contaSQL`" /SQLSVCPASSWORD=`"$contaSQLPWD`" /AGTSVCACCOUNT=`"$contaAgente`" /AGTSVCPASSWORD=`"$contaAgentePWD`" /AGTSVCSTARTUPTYPE=`"Automatic`" /SAPWD=`"$senhaSA`" /SECURITYMODE=SQL /SQLBACKUPDIR=`"$pastaBKP`" /SQLCOLLATION=`"$collation`" /SQLSVCSTARTUPTYPE=`"Automatic`" /SQLSYSADMINACCOUNTS=$admins /SQLTEMPDBDIR=`"$pastaTEMPDB`" /SQLTEMPDBLOGDIR=`"$pastaTEMPDBLOG`" /SQLUSERDBDIR=`"$pastaDB`" /SQLUSERDBLOGDIR=`"$pastaLOG`" /NPENABLED=1 /TCPENABLED=1' 
           # -Wait
           # "
            #$command = $ExecutionContext.InvokeCommand.NewScriptBlock($command)
            #$a = "$iso_unit\Setup.exe /ACTION=INSTALL /IACCEPTSQLSERVERLICENSETERMS /Q /ENU /FEATURES=TOOLS /INDICATEPROGRESS /INSTANCENAME=INST_004 /FEATURES=SQLENGINE"
            #$command = "Invoke-Expression '$a'"

            #write-output $command 
            #$resultado = invoke-Command -ComputerName $servidor -Credential $credencialinterna -ScriptBlock { whoami /priv }
           
           #$command = "Start-Process $iso_unit\Setup.exe -ArgumentList '/ACTION=INSTALL /IACCEPTSQLSERVERLICENSETERMS /Q /ENU /FEATURES=SQLENGINE,FULLTEXT,TOOLS /INSTANCEDIR=`"$caminhoBinario`" $exibeVerbose /INSTANCENAME=`"$instancia`" /PID=`"$chaveProduto`" /INSTALLSQLDATADIR=`"$pastaArquivos`"     /SQLSVCACCOUNT=`"$contaSQL`" /SQLSVCPASSWORD=`"$contaSQLPWD`" /AGTSVCACCOUNT=`"$contaAgente`" /AGTSVCPASSWORD=`"$contaAgentePWD`" /AGTSVCSTARTUPTYPE=`"Automatic`" /SAPWD=`"$senhaSA`" /SECURITYMODE=SQL /SQLBACKUPDIR=`"$pastaBKP`" /SQLCOLLATION=`"$collation`" /SQLSVCSTARTUPTYPE=`"Automatic`"  /SQLTEMPDBDIR=`"$pastaTEMPDB`" /SQLTEMPDBLOGDIR=`"$pastaTEMPDBLOG`" /SQLUSERDBDIR=`"$pastaDB`" /SQLUSERDBLOGDIR=`"$pastaLOG`" /NPENABLED=1 /TCPENABLED=1' -Wait"
           #$command = $ExecutionContext.InvokeCommand.NewScriptBlock($command)
           #$resultado = invoke-Command -ComputerName $servidor -Credential $credencialinterna -ScriptBlock $command
           
           $a = "$iso_unit\Setup.exe /ACTION=INSTALL /IACCEPTSQLSERVERLICENSETERMS /Q /ENU /FEATURES=SQLENGINE,FULLTEXT,TOOLS /INSTANCEDIR=`"$caminhoBinario`" $exibeVerbose /INSTANCENAME=`"$instancia`" /PID=`"$chaveProduto`" /INSTALLSQLDATADIR=`"$pastaArquivos`" /SQLSVCACCOUNT=`"$contaSQL`" /SQLSVCPASSWORD=`"$contaSQLPWD`" /AGTSVCACCOUNT=`"$contaAgente`" /AGTSVCPASSWORD=`"$contaAgentePWD`" /AGTSVCSTARTUPTYPE=`"Automatic`" /SAPWD=`"$senhaSA`" /SECURITYMODE=SQL /SQLBACKUPDIR=`"$pastaBKP`" /SQLCOLLATION=`"$collation`" /SQLSVCSTARTUPTYPE=`"Automatic`" /SQLSYSADMINACCOUNTS=$admins  /SQLTEMPDBDIR=`"$pastaTEMPDB`" /SQLTEMPDBLOGDIR=`"$pastaTEMPDBLOG`" /SQLUSERDBDIR=`"$pastaDB`" /SQLUSERDBLOGDIR=`"$pastaLOG`" /NPENABLED=1 /TCPENABLED=1"
           $command = "Invoke-Expression '$a'"
           write-output $command
           $command = $ExecutionContext.InvokeCommand.NewScriptBlock($command)
           $resultado = invoke-Command -ComputerName $servidor -Credential $credencialinterna -ScriptBlock $command 
            
            write-output "Output Instalacao das tools SQL: $resultado"
        }
        catch{
            $resultado = $False
            throw "Step 2 - Instalacao das tools SQL $_.Exception"
        }
     } 
    
}